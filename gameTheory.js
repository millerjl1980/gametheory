
function showOnScreen(divID, infoToDisplay){
    let element = document.createElement("element");
    let items = document.createTextNode(infoToDisplay);
    element.appendChild(items);
    let destination= document.getElementById(divID);
    destination.appendChild(element);


}
function clearBox(){
    document.getElementById("computerTurn").innerHTML = "";
    document.getElementById("winner").innerHTML = "";
}

let gameChoices = [null, "Scissors", "Paper", "Rock", "Lizard", "Spock"];
let player1 = "";
let computerChoice = "";
let winner = "";

function computerTurn(){
    computerChoice = "";
    let chance = Math.ceil(Math.random() * (gameChoices.length-1));
    computerChoice = gameChoices[chance];
    showOnScreen("computerTurn", computerChoice);
    return computerChoice;
}

let player1Choice = document.querySelector("#userChoices");
player1Choice.addEventListener("click", userChoice, false);

function userChoice(click){
    if (click.target !== click.currentTarget){
        player1 = click.target.id;
        clearBox();
        computerTurn();
        whoWins(player1, computerChoice);
    }
}


function whoWins(player1, computerChoice){
    switch (player1){
        case "Scissors":
            if (computerChoice === "Paper"){
               winner = "Scissors cuts Paper";
               break;
            }else if(computerChoice === "Lizard"){
                winner = "Scissors decaptiates Lizard";
                break;
            }else if(computerChoice === "Spock"){
                winner = "Spock smashes scissors";
                break;
            }else if(computerChoice === "Rock"){
                winner = "Rock crushes Scissors";
                break;
            }else 
                winner = "Draw, try again";
                break;
        case "Paper":
            if (computerChoice === "Scissors"){
                winner = "Scissors cuts Paper";
                break;
            }else if(computerChoice === "Lizard"){
                winner = "Lizard eats Paper";
                break;
            }else if(computerChoice === "Spock"){
                winner = "Paper disproves Spock";
                break;
            }else if(computerChoice === "Rock"){
                winner = "Paper Covers Rock";
                break;
            }else
                winner = "Draw, try again";
                break;
        case "Rock":
            if (computerChoice === "Scissors"){
                winner = "Rock crushes Scissors";
                break;
            }else if(computerChoice === "Lizard"){
                winner = "Rock crushes Lizard";
                break;
            }else if(computerChoice === "Spock"){
                winner = "Spock vaporizes Rock";
                break;
            }else if(computerChoice === "Paper"){
                winner = "Paper covers Rock";
                break;
            }else
                winner = "Draw, try again";
                break;
        case "Lizard":
            if (computerChoice === "Scissors"){
                winner = "Scissors decapitates Lizard";
                break;
            }else if(computerChoice === "Rock"){
                winner = "Rock crushes Lizard";
                break;
            }else if(computerChoice === "Spock"){
                winner = "Lizard poisons Spock";
                break;
            }else if(computerChoice === "Paper"){
                winner = "Lizard eats Paper";
                break;
            }else
                winner = "Draw, try again"; 
                break;   
        case "Spock":
            if (computerChoice === "Scissors"){
                winner = "Spock smashes Scissors";
                break;
            }else if(computerChoice === "Rock"){
                winner = "Spock vaporizes Rock";
                break;
            }else if(computerChoice === "Lizard"){
                winner = "Lizard poisons Spock";
                break;
            }else if(computerChoice === "Paper"){
                winner = "Paper disproves Spock";
                break;
            }else
                winner = "Draw, try again";
                break;
    }
    showOnScreen("winner", winner);
}